package com.sample;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.quarkus.security.identity.SecurityIdentity;

@Path("/users")
public class HelloResource {

	@Inject
	SecurityIdentity identity;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/acc")
	@RolesAllowed("user")
	public SecurityIdentity getUSerInfo() {
		return identity;
	}
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }
    
}