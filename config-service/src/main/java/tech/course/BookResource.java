package tech.course;

import javax.ws.rs.core.MediaType;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import tech.course.data.Book;
import tech.course.service.BookService;

@Path("/book")
public class BookResource {
	@Inject
	 BookService bookService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List <Book> getBooks() {
		return bookService.getAllBooks();
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	
	public Book addBook(Book book) {
		return bookService.saveBook(book);
	}
	
	@PUT
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book updateBook(@PathParam("id")int id,Book book) {
		return bookService.updateBook(id, book);
		
	}

	@DELETE
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book deleteBook(@PathParam("id") int id) {
		
		return bookService.deleteBook(id);
	}
}
