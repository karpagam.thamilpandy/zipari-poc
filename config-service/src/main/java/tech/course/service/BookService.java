package tech.course.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import io.netty.util.internal.StringUtil;
import tech.course.data.Book;

@Transactional
@ApplicationScoped
public class BookService {

	
	/*
	 * private static ArrayList<String> books=new ArrayList<String>(); static {
	 * books.add("Learn Quarkus for Beginners"); } public String getBooks() { return
	 * StringUtil.join(",", books).toString();
	 * 
	 * } public String addBook(String book) { books.add(book); return book; } public
	 * String updateBook(int id,String book) { books.remove(id); books.add(id,
	 * book); return book; } public String deleteBook(int id) { return
	 * books.remove(id);
	 * 
	 * }
	 */
	@PersistenceContext 
	EntityManager manager;
	
	public Book saveBook(Book book) {
		manager.persist(book);
		return book;
	}
	
	public List<Book> getAllBooks(){
		return manager.createQuery("select b from Book b",Book.class).getResultList();
	}
	
	public Book updateBook(int id,Book book) {
		manager.merge(book);
		return book;

	}
	
	public Book deleteBook(int id) {
		Book book=manager.find(Book.class, id);
		manager.remove(book);
		return book;
	}
}
