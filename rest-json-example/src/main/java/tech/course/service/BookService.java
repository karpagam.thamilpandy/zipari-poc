package tech.course.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import tech.course.data.Book;

@ApplicationScoped
public class BookService {
	ArrayList<Book> books=new ArrayList<Book>();

	public Collection<Book> getBooks(){
		return books;
	}
	
	public Book addBook(Book book) {
		books.add(book);
		return book;
		
	}
}
