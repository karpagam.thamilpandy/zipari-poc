package tech.course;

import javax.ws.rs.core.MediaType;
import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import tech.course.data.Book;
import tech.course.service.BookService;

@Path("/book")
public class BookResource {

	@Inject
	BookService bookService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Book> getBooks(){
		return bookService.getBooks();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Book addBook(Book book) {
		return bookService.addBook(book);
	}
}
