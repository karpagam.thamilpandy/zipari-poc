package tech.course.data;


public class Book {

	private String name;
	private String author;
	private Long pages;
	
	public Book() {
		
	}
	public Book(String name, String author, Long pages) {
		super();
		this.name = name;
		this.author = author;
		this.pages = pages;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Long getPages() {
		return pages;
	}
	public void setPages(Long pages) {
		this.pages = pages;
	}
	
	
}
