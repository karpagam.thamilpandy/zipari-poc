package com.tech.training;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.vertx.ConsumeEvent;
@ApplicationScoped
public class GreetingService {

	@ConsumeEvent("greeting")
    public String greeting(String name) {
        return "Hello " + name;
    }
}
